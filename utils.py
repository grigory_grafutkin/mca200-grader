import spur

# keystone = keystone_client.Client(username='admin', password='sometimeheading', tenant_name='admin', auth_url='http://172.15.0.202:5000/v2.0')
# nova = nova_client.Client('2', 'admin', 'sometimeheading', 'admin', 'http://172.15.0.202:5000/v2.0')
# cinder = cinder_client.Client('2', 'admin', 'sometimeheading', 'admin', 'http://172.15.0.202:5000/v2.0')
# neutron = neutron_client.Client(username='admin', password='sometimeheading', tenant_name='admin',
#                                 auth_url='http://172.15.0.202:5000/v2.0')
# glance = glance_client.Client('2', endpoint='http://172.15.0.202:9292', token=keystone.auth_token)


from Levenshtein import distance
from components.clients import Clients


admin = Clients('172.15.0.202', tenant_name='marketing')
marketing = Clients('172.15.0.202', tenant_name='marketing')
finance = Clients('172.15.0.202', tenant_name='finance')
james = Clients('172.15.0.202', tenant_name='marketing', username='james', password='jamespass')

tenants = {
    'admin': admin,
    'marketing': marketing,
    'finance': finance
}


def similar(str1, str2, dist=2):
    return distance(str(str1), str(str2)) < dist


def similar_name_in(value, iterable):
    for item in iterable:
        if isinstance(value, tuple):
            if value[0] == 'name':
                if similar(item, (unicode(value[0]), unicode(value[1]))):
                    return True
        if item == value:
            return True
    return False


def find_tenant(_tenant_name):
    for t in [t for t in admin.keystone.tenants.list() if similar(t.name, _tenant_name)]:
        return t


def find_user(_user_name, _tenant):
    for u in [u for u in _tenant.list_users() if similar(u.username, _user_name)]:
        return u


def find_role(_role, _user, _tenant):
    for r in [r for r in admin.keystone.roles.roles_for_user(_user, _tenant) if similar(r.name, _role)]:
        return r


def get_quotes(_tenant):
    return dict(admin.nova.quotas.get(find_tenant(_tenant).id)._info.items() +
                  admin.cinder.quotas.get(find_tenant(_tenant).id)._info.items() +
                  admin.neutron.show_quota(find_tenant(_tenant).id)['quota'].items())


def find_images():
    return admin.glance.images.list()


def find_flavors(tenant_name='admin'):
    return tenants[tenant_name].nova.flavors.list()


def ssh(command, allow_error=True):
    shell = spur.SshShell(hostname='172.15.0.202', username='stack', password='sometimeheading',
                          missing_host_key=spur.ssh.MissingHostKey.accept)
    with shell:
        result = shell.run(command, allow_error=allow_error)
    return result.output
