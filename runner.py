from components.lab import Lab
from data import clabs
from importlib import import_module
from components.logger import loggers
import warnings

warnings.simplefilter("ignore", UserWarning)

# module_list = ['files', 'flavors', 'images', 'instances', 'keypairs', 'networks', 'objects', 'quotas', 'routers',
#                'secgroups', 'tenants', 'users', 'volumes']
list_of_all_modules = ['tenants', 'users', 'volumes']


class Runner:
    def __init__(self, labs, module_list=None):
        self._labs = {}
        if not module_list:
            module_list = list_of_all_modules
        # module data is a tuple of imported module and capitalized name comfortable for instantiating
        # e.g. [(<Files>, 'Files'), (<Tenants>, 'Tenants')..]
        self._module_data = [(import_module('modules.' + module), module.capitalize()) for module in module_list]

        for lab_id in labs:
            clab = [v for k, v in clabs.iteritems() if k == int(lab_id)][0]
            self._labs[lab_id] = Lab(clab['ip'], clab['name'])


    def run_all_modules(self, lab):
        log = loggers.get_logger(str(lab))
        # instantiate all modules
        modules = [getattr(data[0], data[1])(lab) for data in self._module_data]
        lab.results['Total'] = {'result_points': 0, 'max_points': 0}
        for task in modules:
            task.grade()
            res = task.get_score()
            lab.results['Total']['result_points'] += res['result_points']
            lab.results['Total']['max_points'] += res['max_points']
            lab.results[str(task)] = res
        log.info('{r}/{m}'.format(r=lab.results['Total']['result_points'], m=lab.results['Total']['max_points']))
        return lab.results


    def run_single_module(self, task, lab):
        log = loggers.get_logger(str(lab))
        task.set_lab(lab)
        task.grade()
        results = task.get_score()

    def run_all_wide(self):
        for l in self._labs:
            self.run_all_modules(self._labs[l])
        self.output_lab_results(self._labs)

    def output_lab_results(self, labs):
        log = loggers.get_logger('main')
        log.info(','.join([str(task[1]) for task in self._module_data] + ['Total', 'Percentage']))
        for lab_id, lab in labs.iteritems():
            total_string = '{r}/{m}'.format(r=lab.results['Total']['result_points'], m=lab.results['Total']['max_points'])
            log.info(','.join(
                ['{}/{}'.format(lab.results[str(task[1])]['result_points'], lab.results[str(task[1])]['max_points'])
                 for task in self._module_data] +
                [total_string, '{}%'.format(str(int(float(lab.results['Total']['result_points']) / float(lab.results['Total']['max_points']) * 100)))]))


    @staticmethod
    def run(task):
        # n = raw_input('Please enter number of the lab:\n')
        n = 2
        clab = [v for k, v in clabs.iteritems() if k == int(n)][0]
        lab = Lab(clab['ip'], clab['name'])
        runner = Runner([2])
        runner.run_single_module(task, lab)


if __name__ == '__main__':
    runner = Runner([1, 2])
    runner.run_all_wide()
