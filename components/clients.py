from novaclient import client as nova_client
from keystoneclient.v2_0 import client as keystone_client
from cinderclient import client as cinder_client
from neutronclient.v2_0 import client as neutron_client
from glanceclient import client as glance_client
from swiftclient import client as swift_client

tenants_admin = {
    'admin': {
        'username': 'admin',
        'password': 'sometimeheading',
    },
    'marketing': {
        'username': 'roger',
        'password': 'rogerpass'
    },
    'finance': {
        'username': 'frank',
        'password': 'frankpass'
    }
}


class Clients:
    def __init__(self, host, tenant_name='admin', username=None, password=None):
        self._tenant_name = tenant_name
        if username and password:
            self._username = username
            self._password = password
        else:
            self._username = tenants_admin[tenant_name]['username']
            self._password = tenants_admin[tenant_name]['password']
        self._host = host
        self._keystone = None
        self._nova = None
        self._neutron = None
        self._cinder = None
        self._glance = None
        self._swift = None

    @property
    def tenant(self):
        return self._tenant_name

    @property
    def keystone(self):
        if self._keystone:
            return self._keystone
        else:
            self._keystone = keystone_client.Client(username=self._username, password=self._password,
                                                    tenant_name=self._tenant_name,
                                                    auth_url='http://%s:5000/v2.0' % self._host, timeout=10)
            return self._keystone

    @property
    def nova(self):
        if self._nova:
            return self._nova
        else:
            self._nova = nova_client.Client('2', self._username, self._password, self._tenant_name,
                                            'http://%s:5000/v2.0' % self._host)
            return self._nova

    @property
    def neutron(self):
        if self._neutron:
            return self._neutron
        else:
            self._neutron = neutron_client.Client(username=self._username, password=self._password,
                                                  tenant_name=self._tenant_name,
                                                  auth_url='http://%s:5000/v2.0' % self._host)
            return self._neutron

    @property
    def glance(self):
        if self._glance:
            return self._glance
        else:
            self._glance = glance_client.Client('2', endpoint='http://%s:9292' % self._host,
                                                token=self.keystone.auth_token)
            return self._glance

    @property
    def cinder(self):
        if self._cinder:
            return self._cinder
        else:
            self._cinder = cinder_client.Client('2', self._username, self._password, self._tenant_name,
                                                'http://%s:5000/v2.0' % self._host)
            return self._cinder

    @property
    def swift(self):
        if self._swift:
            return self._swift
        else:
            self._swift = swift_client.Connection(user=self._username, key=self._password,
                                                  tenant_name=self._tenant_name,
                                                  authurl='http://%s:5000/v2.0' % self._host, auth_version='2')
            return self._swift
