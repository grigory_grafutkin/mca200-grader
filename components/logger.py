import logging
import sys


class Loggers:

    def __init__(self):
        self._loggers = {}

    def get_logger(self, lab_name):
        if lab_name in self._loggers:
            return self._loggers[lab_name]
        else:
            logger = logging.getLogger(lab_name)
            logger.setLevel(logging.DEBUG)
            streamHandler = logging.StreamHandler(stream=sys.stdout)
            streamHandler.setLevel(logging.INFO)
            streamHandler.setFormatter(logging.Formatter('{lab:7} %(levelname)-7s %(message)s'.format(lab=lab_name)))
            logger.addHandler(streamHandler)
            self._loggers[lab_name] = logger
            return logger

loggers = Loggers()
