from components.clients import Clients
import spur
from utils import similar


class Lab:
    def __init__(self, host, name):
        # init
        self._host = host
        self._name = name
        self._results = {}
        self._admin = Clients(host, tenant_name='marketing')
        self._marketing = Clients(host, tenant_name='marketing')
        self._finance = Clients(host, tenant_name='finance')
        self._james = Clients(host, tenant_name='marketing', username='james', password='jamespass')

        self._tenants = {
            'admin': self.admin,
            'marketing': self.marketing,
            'finance': self.finance
        }

    def __str__(self):
        return self._name


    @property
    def results(self):
        return self._results

    @property
    def admin(self):
        return self._admin

    @property
    def marketing(self):
        return self._marketing

    @property
    def finance(self):
        return self._finance

    @property
    def james(self):
        return self._james

    @property
    def tenants(self):
        return self._tenants

    def find_tenant(self, tenant_name):
        for t in [t for t in self.admin.keystone.tenants.list() if similar(t.name, tenant_name)]:
            return t

    def find_user(self, user_name, tenant):
        for u in [u for u in tenant.list_users() if similar(u.username, user_name)]:
            return u

    def find_role(self, role, user, tenant):
        for r in [r for r in self.admin.keystone.roles.roles_for_user(user, tenant) if similar(r.name, role)]:
            return r

    def find_images(self):
        return self.admin.glance.images.list()

    def find_flavors(self, tenant_name='admin'):
        return self.tenants[tenant_name].nova.flavors.list()

    def get_quotes(self, tenant):
        return dict(self.admin.nova.quotas.get(self.find_tenant(tenant).id)._info.items() +
                    self.admin.cinder.quotas.get(self.find_tenant(tenant).id)._info.items() +
                    self.admin.neutron.show_quota(self.find_tenant(tenant).id)['quota'].items())

    def ssh(self, command, allow_error=True):
        shell = spur.SshShell(hostname=self._host, username='stack', password='sometimeheading',
                              missing_host_key=spur.ssh.MissingHostKey.accept)
        with shell:
            result = shell.run(command, allow_error=allow_error)
        return result.output
