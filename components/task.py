import logging
from logger import loggers


class Task:
    def __init__(self, lab=None):
        self._passed = 0
        self._failed = 0
        self._max_points = 30
        self._lab = None
        self._logger = None
        self._errors = []
        if lab:
            self.set_lab(lab)

    def __str__(self):
        return self.__class__.__name__

    @property
    def max_points(self):
        return self._max_points

    def set_max_points(self, max_points):
        self._max_points = max_points

    @property
    def lab(self):
        return self._lab

    def set_lab(self, lab):
        self._lab = lab
        self._logger = loggers.get_logger(str(lab))

    def passed(self, msg=None):
        if msg:
            self.log.debug(msg)
        self._passed += 1

    def failed(self, msg=None):
        if msg:
            self.log.error(msg)
            self._errors.append(msg)
        self._failed += 1

    def before_grade(self):
        self._logger.info('Grading module {m}'.format(m=str(self)))

    def after_grade(self):
        res = self.get_score()
        self._logger.info(
            '{m} results: {r}/{max}'.format(m=str(self), r=res['result_points'], max=res['max_points']))

    def get_score(self):
        percentage = self._passed / (float(self._passed) + float(self._failed))
        return {'result_points': int(percentage * self._max_points),
                'max_points': self._max_points,
                'error_list': self._errors}

    @property
    def log(self):
        return self._logger
