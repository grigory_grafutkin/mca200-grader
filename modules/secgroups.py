from components.task import Task


class Secgroups(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(20)
        self._reference = [
            {
                'tenant': 'marketing',
                'name': 'msec',
                'security_group_rules': [
                    {
                        'direction': 'ingress',
                        'remote_ip_prefix': '0.0.0.0/0',
                        'protocol': 'tcp',
                        'port_range_min': 443,
                        'port_range_max': 443,
                        'ethertype': 'IPv4'
                    },
                    {
                        'direction': 'ingress',
                        'remote_ip_prefix': '0.0.0.0/0',
                        'protocol': 'tcp',
                        'port_range_min': 80,
                        'port_range_max': 80,
                        'ethertype': 'IPv4'
                    },
                    {
                        'direction': 'ingress',
                        'remote_ip_prefix': '0.0.0.0/0',
                        'protocol': 'tcp',
                        'port_range_min': 22,
                        'port_range_max': 22,
                        'ethertype': 'IPv4'
                    },
                    {
                        'direction': 'ingress',
                        'remote_ip_prefix': '0.0.0.0/0',
                        'protocol': 'icmp',
                        'ethertype': 'IPv4'
                    }
                ]
            },
            {
                'tenant': 'finance',
                'name': 'fsec',
                'security_group_rules': [
                    {
                        'direction': 'ingress',
                        'remote_ip_prefix': '0.0.0.0/0',
                        'protocol': 'tcp',
                        'port_range_min': 22,
                        'port_range_max': 22,
                        'ethertype': 'IPv4'
                    }
                ]
            }
        ]

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        for ref_secgroup in self._reference:
            ref_tenant_id = self.lab.find_tenant(ref_secgroup['tenant']).id
            self.log.debug('Looking for security group "{s}"'.format(s=ref_secgroup['name']))
            secgroup = filter(
                lambda s: s['name'].lower() == ref_secgroup['name'].lower() and s['tenant_id'] == ref_tenant_id,
                self.lab.admin.neutron.list_security_groups()['security_groups'])
            if secgroup:
                self.log.debug('Security group "{s}" was found'.format(s=ref_secgroup['name']))
                ref_rules = ref_secgroup['security_group_rules']
                rules = secgroup[0]['security_group_rules']
                not_found_rules = list(ref_rules)
                self.log.debug('Checking security rules')
                for ref_rule in ref_rules:
                    for rule in rules:
                        if all(item in rule.iteritems() for item in ref_rule.iteritems()):
                            not_found_rules.remove(ref_rule)
                            self.passed('OK. Correct rule "{r}" was found in security group "{s}"'.format(
                                s=ref_secgroup['name'],
                                r=ref_rule['port_range_min'] if 'port_range_min' in ref_rule else ref_rule['protocol']))
                if not_found_rules:
                    self.failed('Following rules were not found in "{s}" secgroup: {r}'.format(s=ref_secgroup['name'],
                                                                                               r=not_found_rules))
            else:
                self.failed('Security group "{s}" not found in "{t}" tenant'.format(s=ref_secgroup['name'],
                                                                                    t=ref_secgroup['tenant']))
        Task.after_grade(self)
        return Task.get_score(self)

if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner

    Runner.run(Secgroups())
