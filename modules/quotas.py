from components.task import Task


class Quotas(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(20)
        self._reference = {
            'marketing':
                {
                    'cores': 3,
                    'ram': 3072,
                    'gigabytes': 3,
                    'floatingip': 2,
                    'security_group': 2
                },
            'finance':
                {
                    'instances': 2,
                    'gigabytes': 2,
                    'floatingip': 2,
                    'security_group': 2
                }
        }

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        for tenant, expected in self._reference.iteritems():
            # gather all quotas from all openstack services
            self.log.debug('Collecting all quotas of tenant "{t}"'.format(t=tenant))
            actual = self.lab.get_quotes(tenant)
            self.log.debug('Checking values of quotas')
            if all(item in actual.iteritems() for item in expected.iteritems()):
                self.passed('OK. Tenant "{t}" has correct quotas'.format(t=tenant))
            else:
                expected_data = [item for item in expected.iteritems() if item not in actual.iteritems()]
                actual_data = [(item[0], actual[item[0]]) for item in expected.iteritems() if
                               item not in actual.iteritems()]
                self.failed('Tenant "{t}" has wrong quotas: {a}, expected: {e}'.format(t=tenant, a=actual_data,
                                                                                       e=expected_data))
        Task.after_grade(self)
        return Task.get_score(self)

if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner

    Runner.run(Quotas())
