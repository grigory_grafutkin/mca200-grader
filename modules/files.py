from time import sleep

from components.task import Task
from cinderclient import exceptions as cinder_exceptions


class Files(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(20)

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        try:
            self.log.debug('Trying to create volume using unprivileged user')
            vol = self.lab.james.cinder.volumes.create(name='test', size=1)
            # volume was created so we need to delete it after it become available and report that test was failed
            retries = 0
            while self.lab.james.cinder.volumes.get(vol.id).status not in ('available', 'error') and retries < 15:
                sleep(2)
                retries += 2
            self.lab.james.cinder.volumes.delete(vol)
            self.failed('Policies for cinder were not enabled')
        except cinder_exceptions.Forbidden:
            self.passed('OK. Polices\'re in place. Was not able to create volume using unprivileged user')
        except Exception as e:
            self.failed('Failed to verify cinder policies: {msg}'.format(msg=e.message))
        Task.after_grade(self)
        return Task.get_score(self)


if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner
    Runner.run(Files())