from components.task import Task


class Objects(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(15)
        self._reference = {
            'finance': {
                'Movies': [
                    'test.mov'
                ]
            }
        }

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        for tenant, containers in self._reference.iteritems():
            for container, ref_objects in containers.iteritems():
                self.log.debug('Looking for container "{c}"'.format(c=container))
                objects = self.lab.tenants[tenant].swift.get_container(container)
                if objects:
                    self.log.debug('Container "{c}" was found'.format(c=container))
                    for ref_object in ref_objects:
                        self.log.debug('Looking for object "{o}" in container'.format(o=ref_object))
                        if ref_object in [obj['name'] for obj in objects[1]]:
                            self.passed('OK. Object "{o}" was found in storage'.format(o=ref_object))
                        else:
                            self.failed(
                                'Object "{o}" was not found in "{c}" container'.format(o=ref_object, c=container))
        Task.after_grade(self)
        return Task.get_score(self)

if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner
    Runner.run(Objects())
