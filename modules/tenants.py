from components.task import Task


class Tenants(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(10)
        self._reference = ['marketing', 'finance']

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        for ref_tenant in self._reference:
            self.log.debug('Looking for tenant "{t}"'.format(t=ref_tenant))
            if self.lab.find_tenant(ref_tenant):
                self.passed('OK. Tenant "{t}" was found'.format(t=ref_tenant))
            else:
                self.failed('Tenant {t} was not found'.format(t=ref_tenant))
        Task.after_grade(self)
        return Task.get_score(self)

if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner
    Runner.run(Tenants())