from components.task import Task
from utils import similar


class Users(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(20)
        self._reference = {
            'marketing': [
                {
                    'username': 'james',
                    'email': 'james@xamex.tld',
                    'role': 'Member'
                },
                {
                    'username': 'roger',
                    'email': 'roger@xamex.tld',
                    'role': 'admin'
                }
            ],
            'finance': [
                {
                    'username': 'frank',
                    'email': 'frank@xamex.tld',
                    'role': 'admin'
                }
            ]

        }

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        for ref_tenant, users in self._reference.iteritems():
            tenant = self.lab.find_tenant(ref_tenant)
            if tenant:
                for ref_user in users:
                    self.log.debug('Looking for user "{u}"'.format(u=ref_user['username']))
                    user = self.lab.find_user(ref_user['username'], tenant)
                    if user:
                        self.log.debug('User "{u}" was found'.format(u=ref_user['username']))
                        self.log.debug('Checking email')
                        if similar(user.email, ref_user['email']):
                            self.passed(
                                'OK. User "{u}" has correct email "{e}"'.format(u=ref_user['username'],
                                                                                              e=ref_user['email']))
                        else:
                            self.failed('Wrong email in "{u}" user data - actual: "{a}", expected: "{e}"'.format(
                                u=ref_user['username'], a=user.email, e=ref_user['email']))
                        self.log.debug('Checking role in tenant "{t}"'.format(t=ref_tenant))
                        role = self.lab.find_role(ref_user['role'], user, tenant)
                        if role:
                            self.passed(
                                'OK. User "{u}" has correct role "{r}" in tenant "{t}"'.format(u=ref_user['username'],
                                                                                               r=ref_user['role'],
                                                                                               t=ref_tenant))
                        else:
                            print 'User "{u}" has no role "{r}" in "{t}" tenant'.format(u=ref_user['username'],
                                                                                        r=ref_user['role'],
                                                                                        t=ref_tenant)
                            self.failed()
                    else:
                        print 'User "{u}" was not found in "{t}" tenant'.format(u=ref_user['username'], t=ref_tenant)
                        self.failed()
            else:
                print 'Tenant "{t}" was not found'.format(t=ref_tenant)
                self.failed()
        Task.after_grade(self)
        return Task.get_score(self)


if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner

    Runner.run(Users())
