from components.task import Task
from utils import similar_name_in


class Images(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(10)
        self._reference = {
            'visibility': 'public',
            'name': 'sharedimage1'
        }

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        image_found = False
        self.log.debug('Looking for image "{i}"'.format(i=self._reference['name']))
        for image in self.lab.find_images():
            if all(similar_name_in(item, image.items()) for item in self._reference.items()):
                image_found = True
        if image_found:
            self.passed('OK. Image "{i}" was found'.format(i=self._reference['name']))
        else:
            self.failed('Image "{i}" was not found'.format(i=self._reference['name']))
        Task.after_grade(self)
        return Task.get_score(self)


if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner
    Runner.run(Images())
