from components.task import Task


class Routers(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(30)
        self._reference = {
            'mrouter': [
                'public',
                'mnet'
            ],
            'frouter': [
                'public',
                'fnet'
            ]
        }

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        for ref_router, ref_networks in self._reference.iteritems():
            self.log.debug('Looking for router "{r}"'.format(r=ref_router))
            router = filter(lambda r: r['name'] == ref_router, self.lab.admin.neutron.list_routers()['routers'])
            if router:
                self.log.debug('Router "{r}" was found'.format(r=ref_router))
                # find networks and get network id
                # test assumes that all networks are created
                self.log.debug('Looking for networks')
                ref_networks_id = [net['id'] for net in self.lab.admin.neutron.list_networks()['networks'] if
                                   net['name'] in ref_networks]
                router_id = router[0]['id']
                # get actual networks that are plugged into router
                self.log.debug('Retrieving list of networks plugged into router and comparing data')
                router_networks = [r['network_id'] for r in self.lab.admin.neutron.list_ports()['ports'] if
                                   r['device_id'] == router_id]
                # verify that required networks are plugged into router
                if all(net in router_networks for net in ref_networks_id):
                    self.passed('OK. Router "{r}" was found and has correct networks {n} attached'.format(
                        r=ref_router, n=ref_networks))
                else:
                    missing_networks = [net for net in ref_networks_id if net not in router_networks]
                    missing_networks_names = [net['name'] for net in self.lab.admin.neutron.list_networks()['networks']
                                              if
                                              net['id'] in missing_networks]
                    self.failed('Following networks were not connected to "{r}" router: {n}'.format(
                        r=ref_router, n=missing_networks_names))
            else:
                self.failed('Router {r} was not found'.format(r=ref_router))
        Task.after_grade(self)
        return Task.get_score(self)


if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner

    Runner.run(Routers())
