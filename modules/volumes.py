from components.task import Task


class Volumes(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(20)
        self._reference = {
            'marketing': [
                {
                    'volume': 'mvolume',
                    'size': 1,
                    'instance': 'minstance1'
                }
            ]
        }

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        for tenant, ref_volumes in self._reference.iteritems():
            volumes = self.lab.tenants[tenant].cinder.volumes.list()
            if volumes:
                for ref_volume in ref_volumes:
                    self.log.debug('Looking for volume "{v}"'.format(v=ref_volume['volume']))
                    volume = [vol for vol in volumes if ref_volume['volume'] == vol.name]
                    if volume:
                        self.log.debug('Volume "{v}" was found'.format(v=ref_volume['volume']))
                        self.log.debug('Checking volume size')
                        if volume[0].size == ref_volume['size']:
                            self.passed(
                                'OK. Volume "{v}" was found and has correct size "{s}"'.format(v=ref_volume['volume'],
                                                                                               s=ref_volume['size']))
                        else:
                            self.failed('Volume "{v}" has wrong size, expected: "{e}", actual: "{a}"'.format(
                                v=ref_volume['volume'],
                                e=ref_volume['size'],
                                a=volume[0].size))
                        self.log.debug('Checking volume\'s attachments')
                        if volume[0].attachments:
                            for attachment in volume[0].attachments:
                                instance = self.lab.admin.nova.servers.get(attachment['server_id'])
                                if ref_volume['instance'] == instance.name:
                                    self.passed('OK. Volume "{v}" has been attached to correct instance "{i}"'.format(
                                        v=ref_volume['volume'], i=ref_volume['instance']))
                                else:
                                    self.failed('Volume "{v}" was attached to wrong instance. expected: "{e}", actual: '
                                                '"{a}"'.format(v=ref_volume['volume'], e=ref_volume['instance'],
                                                               a=instance.name))
                        else:
                            self.failed('Volume "{v}" was not attached'.format(v=ref_volume['volume']))

                    else:
                        self.failed('Volume "{v}" was not found in "{t}" tenant'.format(v=ref_volume['volume'], t=tenant))

            else:
                print 'No volumes found in "{t}"'.format(t=tenant)
                self.failed()
        Task.after_grade(self)
        return Task.get_score(self)


if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner

    Runner.run(Volumes())
