from components.task import Task
from utils import similar, similar_name_in


class Networks(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(30)
        self._reference = [
            {
                'tenant': 'admin',
                'network': {
                    'name': 'public',
                    'provider:network_type': 'vxlan',
                    'router:external': True,
                    'subnet': {
                        'name': 'publicsubnet',
                        'cidr': '172.24.4.0/24',
                        'gateway_ip': '172.24.4.1',
                        'allocation_pools': [{
                            'start': '172.24.4.242',
                            'end': '172.24.4.250'
                        }]
                    }
                }
            },
            {
                'tenant': 'marketing',
                'network': {
                    'name': 'mnet',
                    'subnet': {
                        'name': 'msubnet',
                        'cidr': '10.1.0.0/24',
                        'gateway_ip': '10.1.0.1',
                    }
                }
            },
            {
                'tenant': 'finance',
                'network': {
                    'name': 'fnet',
                    'subnet': {
                        'name': 'fsubnet',
                        'cidr': '10.2.0.0/24',
                        'gateway_ip': '10.2.0.1',
                    }
                }
            }
        ]

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        for ref_net in self._reference:
            # using tenant_id to verify correct tenant in networks
            ref_tenant_id = self.lab.find_tenant(ref_net['tenant']).id
            # matching network by name and tenant id
            self.log.debug('Looking for network "{n}"'.format(n=ref_net['network']['name']))
            net = filter(lambda n: similar(n['name'], ref_net['network']['name']) and n['tenant_id'] == ref_tenant_id,
                         self.lab.admin.neutron.list_networks()['networks'])
            ref_subnet = ref_net['network']['subnet']
            if net:
                self.log.debug(
                    'Network "{n}" was found in tenant "{t}"'.format(n=ref_net['network']['name'], t=ref_net['tenant']))
                # Checking presence of reference network parameters filtering 'subnet' tree. We cannot compare it.
                # Will fail if any of the parameter differs from reference.
                self.log.debug('Checking network parameters')
                if all(similar_name_in(item, net[0].iteritems()) for item in ref_net['network'].iteritems() if
                       item[0] != 'subnet'):
                    self.passed('OK. Network "{n}" has correct parameters'.format(
                        n=ref_net['network']['name']))
                else:
                    expected_data = [item for item in ref_net['network'].iteritems()
                                     if item not in net[0].iteritems() and item[0] != 'subnet']
                    actual_data = [(item[0], net[0][item[0]]) for item in ref_net['network'].iteritems()
                                   if item not in net[0].iteritems() and item[0] != 'subnet']
                    self.failed('Network "{n}" has wrong parameters, expected: {e}, actual {a}'.format(
                        n=ref_net['network']['name'],
                        e=expected_data,
                        a=actual_data))
                # matching subnet by name and tenant id
                self.log.debug('Looking for subnet "{s}"'.format(s=ref_subnet['name']))
                subnet = filter(lambda s: similar(s['name'], ref_subnet['name']) and s['tenant_id'] == ref_tenant_id,
                                [self.lab.admin.neutron.show_subnet(subnet)['subnet'] for subnet in net[0]['subnets']])
                if subnet:
                    self.log.debug('Subnet "{s}" of network "{n}" was found'.format(s=ref_subnet['name'],
                                                                                    n=ref_net['network']['name']))
                    # Checking presence of reference subnet parameters. Will fail in any doesn't
                    self.log.debug('Checking subnet parameters')
                    if all(similar_name_in(item, subnet[0].iteritems()) for item in ref_subnet.iteritems()):
                        self.passed('OK. Subnet "{s}" has correct parameters'.format(s=ref_subnet['name']))
                    else:
                        expected_data = [item for item in ref_subnet.iteritems() if item not in subnet[0].iteritems()]
                        actual_data = [(item[0], subnet[0][item[0]]) for item in ref_subnet.iteritems() if
                                       item not in subnet[0].iteritems()]
                        self.failed('Subnet "{s}" has wrong parameters, expected: {e}, actual: {a}'.format(
                            s=ref_subnet['name'],
                            e=expected_data,
                            a=actual_data))
                else:
                    self.failed('Subnet "{s}" was not found'.format(s=ref_subnet['name']))
            else:
                self.failed('Network "{n}" was not found in "{t}" tenant'.format(n=ref_net['network']['name'],
                                                                                 t=ref_net['tenant']))
        Task.after_grade(self)
        return Task.get_score(self)

if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner

    Runner.run(Networks())
