from components.task import Task
from utils import similar


class Keypairs(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(15)
        self._reference = {
            'marketing': {
                'keypair': 'rogerkey',
                'keyfile': '/tmp/rogerkey.pem',
                'ssh': [
                    {
                        'cmd': ['ssh', '-o', 'StrictHostKeyChecking=no', '-i', '/tmp/rogerkey.pem',
                                'cirros@172.24.4.243',
                                'hostname'],
                        'result': 'minstance1'
                    },
                    {
                        'cmd': ['ssh', '-o', 'StrictHostKeyChecking=no', '-i', '/tmp/rogerkey.pem',
                                'cirros@172.24.4.244',
                                'hostname'],
                        'result': 'minstance2'
                    }
                ]
            },
            'finance': {
                'keypair': 'frankkey',
                'keyfile': '/tmp/frankkey.pem',
                'ssh': [
                    {
                        'cmd': ['ssh', '-o', 'StrictHostKeyChecking=no', '-i', '/tmp/frankkey.pem',
                                'cirros@172.24.4.246',
                                'hostname'],
                        'result': 'finstance1'
                    }
                ]
            }
        }

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        for k, v in self._reference.iteritems():
            # check key presence in nova database
            self.log.debug('Checking for keypair "{k}" presence in database'.format(k=v['keypair']))
            key_found = False
            for item in self.lab.tenants[k].nova.keypairs.list():
                if similar(item.name, v['keypair']):
                    key_found = True
            if not key_found:
                self.failed('Key "{k}" not found in "{t}" tenant'.format(k=v['keypair'], t=k))
            else:
                self.passed('OK. Correct keypair "{k}" was found in tenant "{t}"'.format(k=v['keypair'], t=k))
            # try verify that .pem file exist
            self.log.debug('Looking for {k} file on the host'.format(k=v['keyfile']))
            if v['keyfile'] in self.lab.ssh(['ls', v['keyfile']]):
                self.passed('OK. Keyfile was found on host at "{k}"'.format(k=v['keyfile']))
                # try to ssh to host
                for check in v['ssh']:
                    self.log.debug('Executing ssh to VM using .pem file')
                    result = self.lab.ssh(check['cmd'])
                    if check['result'] in result:
                        self.passed(
                            'OK. Successfully done ssh to VM "{vm}" using keyfile "{key}"'.format(vm=check['result'],
                                                                                                  key=v['keyfile']))
                    else:
                        self.failed('Error found while trying to execute secure shell using command: "{c}", expected ' \
                                    'results: "{e}", actual result: "{a}"'.format(
                            c=' '.join(check['cmd']), e=check['result'], a=result))
            else:
                self.failed('Keyfile "{k}" was not found'.format(k=v['keyfile']))
        Task.after_grade(self)
        return Task.get_score(self)


if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner

    Runner.run(Keypairs())
