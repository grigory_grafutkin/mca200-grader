from components.task import Task


class Flavors(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(30)
        self._reference = [{
            'tenant': 'marketing',
            'flavors': [
                {
                    'name': 'mflavor1',
                    'vcpus': 1,
                    'ram': 1024,
                    'disk': 2,
                    'OS-FLV-EXT-DATA:ephemeral': 0,
                    'swap': ''
                },
                {
                    'name': 'mflavor2',
                    'vcpus': 2,
                    'ram': 2048,
                    'disk': 2,
                    'OS-FLV-EXT-DATA:ephemeral': 0,
                    'swap': ''
                }
            ]
        }, {
            'tenant': 'finance',
            'flavors': [
                {
                    'name': 'fflavor1',
                    'vcpus': 1,
                    'ram': 512,
                    'disk': 1,
                    'OS-FLV-EXT-DATA:ephemeral': 0,
                    'swap': ''
                }
            ]
        }
        ]

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        for tenant in self._reference:
            for ref_flavor in tenant['flavors']:
                self.log.debug('Looking for flavor "{f}"'.format(f=ref_flavor['name']))
                filtered = filter(lambda f: f.name.lower() == ref_flavor['name'].lower(),
                                  self.lab.find_flavors(tenant['tenant']))
                if filtered:
                    self.log.debug('Flavor "{f}" was found'.format(f=ref_flavor['name']))
                    actual = filtered[0]._info
                    self.log.debug('Cheking flavor\'s parameters')
                    if all(item in actual.items() for item in ref_flavor.items()):
                        self.passed('OK. Tenant "{t}" has correct "{f}" flavor'.format(t=tenant['tenant'],
                                                                                       f=ref_flavor['name']))
                    else:
                        expected_data = [item for item in ref_flavor.iteritems() if item not in actual.iteritems()]
                        actual_data = [(item[0], actual[item[0]]) for item in ref_flavor.iteritems() if
                                       item not in actual.iteritems()]
                        self.failed(
                            'Flavor "{f}" has wrong values, expected: {e}, actual: {a}'.format(f=tenant['tenant'],
                                                                                               e=expected_data,
                                                                                               a=actual_data))
                else:
                    self.failed(
                        'Not found flavor "{f}" in tenant "{t}"'.format(f=ref_flavor['name'], t=tenant['tenant']))
        Task.after_grade(self)
        return Task.get_score(self)


if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner

    Runner.run(Flavors())
