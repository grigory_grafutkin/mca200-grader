from components.task import Task


class Instances(Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self, *args, **kwargs)
        self.set_max_points(60)
        self.reference = {
            'marketing': [
                {
                    'name': 'minstance1',
                    'flavor': 'mflavor1',
                    'keypair': 'rogerkey',
                    'network': 'mnet',
                    'image': 'sharedimage',
                    'secgroup': 'msec',
                    'floatingip': '172.24.4.243'
                },
                {
                    'name': 'minstance2',
                    'flavor': 'mflavor2',
                    'keypair': 'rogerkey',
                    'network': 'mnet',
                    'image': 'sharedimage',
                    'secgroup': 'msec',
                    'floatingip': '172.24.4.244'
                }
            ],
            'finance': [
                {
                    'name': 'finstance1',
                    'flavor': 'fflavor1',
                    'keypair': 'frankkey',
                    'network': 'fnet',
                    'image': 'sharedimage',
                    'secgroup': 'fsec',
                    'floatingip': '172.24.4.246'
                }
            ]
        }

    def grade(self):
        Task.before_grade(self)  # calling common grade code for all tasks
        for ref_tenant, ref_instances in self.reference.iteritems():
            ref_tenenat_id = self.lab.find_tenant(ref_tenant).id
            for ref_instance in ref_instances:
                self.log.debug('Looking for instance "{i}"'.format(i=ref_instance['name']))
                instance = [i for i in self.lab.admin.nova.servers.list(search_opts={'all_tenants': 1}) if
                            i.name == ref_instance['name']]
                if instance:
                    self.log.debug('Instance "{i}" was found'.format(i=ref_instance['name']))
                    flavor = self.lab.admin.nova.flavors.get(instance[0].flavor['id']).name
                    self.log.debug('Checking flavor')
                    if ref_instance['flavor'] == flavor:
                        self.passed('OK. Instance "{i}" has correct flavor "{f}"'.format(i=ref_instance['name'],
                                                                                         f=ref_instance['flavor']))
                    else:
                        self.failed('Wrong flavor used for "{i}" instance, expected: "{e}", actual: "{a}"'.format(
                            i=ref_instance['name'], e=ref_instance['flavor'], a=flavor))
                    self.log.debug('Checking keypairs')
                    if ref_instance['keypair'] == instance[0].key_name:
                        self.passed('OK. Instance "{i}" has correct keypair "{k}"'.format(i=ref_instance['name'],
                                                                                          k=ref_instance['keypair']))
                    else:
                        self.failed('Wrong keypair used for "{i}" instance, expected: "{e}", actual: "{a}"'.format(
                            i=ref_instance['name'], e=ref_instance['keypair'], a=instance[0].key_name))
                    self.log.debug('Checking networks and floating ip')
                    for net, addresses in instance[0].addresses.iteritems():
                        if net == ref_instance['network']:
                            self.passed()
                            if ref_instance['floatingip'] in [a['addr'] for a in addresses]:
                                self.passed(
                                    'OK. Instance "{i}" has correct floating ip "{ip}"'.format(i=ref_instance['name'],
                                                                                               ip=ref_instance[
                                                                                                   'floatingip']))
                            else:
                                self.failed('Floating ip "{f}" was not set to "{i}" instance'.format(
                                    f=ref_instance['floatingip'],
                                    i=ref_instance['name']))
                        else:
                            self.failed('Wrong network found in "{i}" instance'.format(i=ref_instance['name']))
                    self.log.debug('Cheking image')
                    if [image.id == instance[0].image['id'] for image in self.lab.find_images() if
                        image.name == ref_instance['image']]:
                        self.passed(
                            'OK. Instance "{i}" has correct image "{im}" attached'.format(i=ref_instance['name'],
                                                                                          im=ref_instance['image']))
                    else:
                        self.failed('Wrong image used for "{i}" instance, expected: "{e}""'.format(
                            i=ref_instance['name'], e=ref_instance['image']))
                    secgroups = instance[0].list_security_group()
                    self.log.debug('Checking security group')
                    if secgroups:
                        if ref_instance['secgroup'] == instance[0].list_security_group()[0]._info['name']:
                            self.passed(
                                'OK. Instance "{i}" has correct security group "{s}"'.format(i=ref_instance['name'],
                                                                                             s=ref_instance[
                                                                                                 'secgroup']))
                        else:
                            self.failed(
                                'Wrong security group used for "{i}" instance, expected: "{e}", actual: "{a}"'.format(
                                    i=ref_instance['name'], e=ref_instance['secgroup'],
                                    a=instance[0].list_security_group()[0]._info['name']))
                    else:
                        self.failed('No security group defined for "{i}" instance'.format(i=ref_instance['name']))
                else:
                    self.failed(
                        'Instance "{i}" was not found in "{t}" tenant'.format(i=ref_instance['name'], t=ref_tenant))
        Task.after_grade(self)
        return Task.get_score(self)


if __name__ == '__main__':
    # to avoid cyclic import
    from runner import Runner

    Runner.run(Instances())
